# [SECTION] Taking input from user
username = input("Please enter your name:\n")
print(f"Hello {username}! Welcome to the Python Short Course!")

# We use the input() function to retrieve input from the user. '\n' creates a new line for the user to type in
num1 = int(input("Please enter first number:\n"))
num2 = int(input("Please enter second number:\n"))
print(f"The sum of the two inputs is {num1 + num2}.")


# [SECTION] If-else Statements
test_num = 75

if test_num >= 60:
	print("Test passed")
else:
	print("Test failed")


test_num2 = int(input("Please enter the second test number: "))

if test_num2 > 0:
	print("The number is positive")
elif test_num2 == 0:
	print("The number is zero")
else:
	print("The number is negative")


# [SECTION] Loops
# While loop
i = 1

while i <= 5:
	print(f"Current count {i}")
	i += 1


# For loop
fruits = ["apple", "banana", "cherry"]

for fruit in fruits:
	print(fruit)

# range() function
for x in range(6):
	print(f"The current value is {x}")

for x in range(5, 10):
	print(f"The current value is {x}")

for x in range(5, 20, 2):
	print(f"The current value is {x}")


# [SECTION] Break and Continue Statement
# Break Statement
j = 1
while j < 6:
	print(j)
	if j == 3:
		break
	j += 1

# Continue Statement
k = 1
while k < 6:
	k += 1
	if k ==3:
		continue
	print(k)