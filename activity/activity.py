# [Leap Year]
i = 1
while i > 0:
	print()
	year = input("Please enter year: ")
	print()
	if year.isnumeric():
		if int(year) > 0:
			if (int(year) % 4 == 0 and int(year) % 100 == 0 and int(year) % 400 == 0) or (int(year) % 4 == 0 and int(year) % 100 != 0):
				print(f"{year} is a leap year")
				print()
				break
			else:
				print(f"{year} is not a leap year")
				print()
				break
		else:
			print("Input is not a year")
			print()
			continue
	else:
		print("Input is not a year")
		print()
		continue
	i += 1
	print()


# [Row and Column]
row = int(input("Enter number of rows: "))
col = int(input("Enter number of columns: "))
print()
r = 1
c = 1

for r in range(row):
	for c in range(col):
		print("*", end = "")

	print()
